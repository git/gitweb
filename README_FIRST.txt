Girocco README
==============

The "girocco" branch is maintained using TopGit.

It contains the version of gitweb used by Girocco
and may be seen in action at <http://repo.or.cz/>.

TopGit is available from:

  <http://repo.or.cz/topgit/pro.git>

TopGit version 0.19.12 or later is highly recommended.

Once TopGit is installed, the following will set up
a local clone of the girocco gitweb repository for use
with TopGit:

    git clone -b girocco http://repo.or.cz/git/gitweb.git
    cd gitweb
    tg remote --populate origin
    tg info

Use `tg help tg` for more information about TopGit.

Note that while the project name is 'git/gitweb' the entire
Git repository is present even though differences from the
main Git repository are limited to the gitweb subdirectory.

The "release" branch represents the base Git release which
the Girocco-specific gitweb patches have been applied to.

The "release" branch should always point to a commit that
has an official, signed Git release tag.  When Girocco
updates to a newer Git release, the "release" branch is
fast-forwarded to that new release tag and then the
`tg update girocco` command is run to update the "girocco"
staging branch to reflect the change.

For example, to update to the Git release tagged as vX.X.X,
the following procedure would be used:

    git update-ref refs/top-bases/release vX.X.X^0
    tg update girocco

When `tg update girocco` runs it will first merge
refs/top-bases/release into refs/heads/release and then it
will proceed to merge those updates throughout the network of
topics that girocco depends on culminating in an update to
the girocco topic itself.  Along the way any conflicts will
need to be resolved.

The `tg update girocco` process may take some time to run.

To see the list of topics that the girocco branch depends on
and their relationships to one another run this command:

    tg summary --rdeps --exclude=release girocco

(Since all of the topics ultimately depend on release,
omitting those lines makes for a more useful display.)
